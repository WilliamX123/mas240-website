function init() {
	$('body').append('<div id="blackout"><img /><div id="caption"></div></div>');
	$('#caption').css({ 
		height: 'auto',
		padding: 10,
		backgroundColor: 'white',
		margin:'auto',
		marginTop: 5,
		color: 'black'
	}).hide();
	$('#blackout img').hide();
	$('#blackout').hide();
	$('#blackout').css({
		width: '100%',
		height: '100%',
		backgroundColor: 'rgba(0,0,0,0.8)',
		position: 'fixed',
		top: 0,
		left: 0,
		zIndex: 9999
	}).click(function(e){
		e.preventDefault();
		$(this).fadeOut('fast');
		$('body').css({ overflow: 'auto' });
	});
	
	$('.gallery').click(function(e) {
		e.preventDefault();
		$('body').css({ overflow: 'hidden' });
		$('#caption').hide().empty();
		var largerImg = $(this).attr('href');
		var captionTxt = $(this).attr('data-caption');
		$('#blackout img').attr('src', largerImg);
	
		$('#blackout img').load(function() {
			//console.log('image loaded');
			$('#blackout').fadeIn('fast', function() { 
				$('#blackout img').css({ display: 'block', margin: 'auto', marginTop:20, border: '5px solid #fff' });
				if(!captionTxt) {
					$('#caption').hide();
				} else {
					$('#caption').fadeIn('fast').css({ width: $('#blackout img').width()-10 }).html(captionTxt);	
				}
			});
		})
	}).css({
		cursor: 'pointer'
	});
}
$(document).ready(function() { // do not delete this line
	init();
});