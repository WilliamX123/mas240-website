function initGallery() {
	$('.mq-gallery').each(function() {
		$(this)
		.click(displayFS)
		.css({ cursor:'pointer' })
	});
}
function displayFS() {
	var fs = $(this).data('large');
	var caption = $(this).attr('alt');
	$('<div id="blackout"></div>').appendTo('body')
	.css({ 
		backgroundColor: 'black',
		height: '100%',
		width: '100%',
		position: 'fixed',
		top: 0,
		left: 0,
		zIndex: 9999
	});
	$('<div id="closeBlackout">&times;</div>').appendTo('#blackout')
	.css({
		cursor: 'pointer',
		position: 'absolute',
		top: 10,
		right: 20,
		color: 'white',
		fontSize: '3em',
		padding: 10
	})
	.click(function() {
		$(this).parent().remove();
	})
	$('<img src="'+fs+'" id="fs">').appendTo('#blackout')
	.css({
		maxWidth: '80%',
		maxHeight: '80%',
		position: 'absolute',
		top: '50%',
		left: '50%',
		transform: 'translate(-50%,-50%)'
	});
	if(caption) {
		$('<div id="caption">'+caption+'</div>').appendTo('#blackout')
		.css({
			color: 'white',
			fontSize: '1.em',
			position: 'absolute',
			bottom: 20,
			fontFamily: 'Arial',
			textAlign: 'center',
			width: '100%'
		});
	}
}
$(document).ready(function() { // do not delete this line
	initGallery();
});